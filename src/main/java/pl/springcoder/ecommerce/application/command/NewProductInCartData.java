/*
 * Copyright (c) 2018. SpringCoder Lukasz Maslowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.springcoder.ecommerce.application.command;

import java.util.Objects;

public class NewProductInCartData {

    private String shoppingCartId;
    private String productId;
    private int quantity;

    public NewProductInCartData(String shoppingCartId, String productId, int quantity) {
        this.shoppingCartId = shoppingCartId;
        this.productId = productId;
        this.quantity = quantity;
    }

    public NewProductInCartData(String shoppingCartId, String productId) {
        this(shoppingCartId, productId, 1);
    }

    public String getShoppingCartId() {
        return shoppingCartId;
    }

    public String getProductId() {
        return productId;
    }

    public int getQuantity() {
        return quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof NewProductInCartData)) return false;
        NewProductInCartData that = (NewProductInCartData) o;
        return Objects.equals(getShoppingCartId(), that.getShoppingCartId()) &&
                Objects.equals(getProductId(), that.getProductId());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getShoppingCartId(), getProductId());
    }
}
