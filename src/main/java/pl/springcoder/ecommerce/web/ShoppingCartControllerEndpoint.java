package pl.springcoder.ecommerce.web;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import pl.springcoder.ecommerce.application.query.ShoppingCartData;
import pl.springcoder.ecommerce.domain.ProductDoesNotExistException;
import pl.springcoder.ecommerce.domain.ShoppingCartDoesNotExistException;
import pl.springcoder.ecommerce.web.dto.ProductIdData;

@RequestMapping(value = "/api")
public interface ShoppingCartControllerEndpoint {
    @RequestMapping(value = "/carts", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    ShoppingCartData add() throws ShoppingCartDoesNotExistException;


    @RequestMapping(value = "/carts/{id}/products", method = RequestMethod.POST)
    void addProductToShoppingCart(@PathVariable("id") String cartId, @RequestBody ProductIdData productIdData) throws ShoppingCartDoesNotExistException, ProductDoesNotExistException;

    @RequestMapping(value = "/carts/{id}", method = RequestMethod.GET)
    ShoppingCartData find(@PathVariable("id") String id) throws ShoppingCartDoesNotExistException;
}
