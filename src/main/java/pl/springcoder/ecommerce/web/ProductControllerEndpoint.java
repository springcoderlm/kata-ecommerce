package pl.springcoder.ecommerce.web;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import pl.springcoder.ecommerce.application.command.NewProductData;
import pl.springcoder.ecommerce.application.query.ProductData;
import pl.springcoder.ecommerce.domain.ProductAlreadyExistException;
import pl.springcoder.ecommerce.domain.ProductDoesNotExistException;

import java.util.List;

@RequestMapping(value = "/api")
public interface ProductControllerEndpoint {

    @RequestMapping(value = "/products", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public ProductData add(@RequestBody NewProductData newProductData) throws ProductAlreadyExistException,
            ProductDoesNotExistException;

    @RequestMapping(value = "/products", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public List<ProductData> all();

    @RequestMapping(value = "/products/{id}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public ProductData find(@PathVariable("id") String id) throws ProductDoesNotExistException;
}