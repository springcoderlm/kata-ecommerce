/*
 * Copyright (c) 2018. SpringCoder Lukasz Maslowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.springcoder.ecommerce.web;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pl.springcoder.ecommerce.application.ShoppingCartService;
import pl.springcoder.ecommerce.application.command.NewProductInCartData;
import pl.springcoder.ecommerce.application.query.ShoppingCartData;
import pl.springcoder.ecommerce.domain.ProductDoesNotExistException;
import pl.springcoder.ecommerce.domain.ShoppingCartDoesNotExistException;
import pl.springcoder.ecommerce.web.dto.ProductIdData;

@RestController
public class ShoppingCartController implements ShoppingCartControllerEndpoint{

    private ShoppingCartService shoppingCartService;

    public ShoppingCartController(ShoppingCartService shoppingCartService) {
        this.shoppingCartService = shoppingCartService;
    }

    public ShoppingCartData add() throws ShoppingCartDoesNotExistException {
        String id = shoppingCartService.newShoppingCart();
        return shoppingCartService.view(id);
    }

    public void addProductToShoppingCart(@PathVariable("id") String cartId, @RequestBody ProductIdData productIdData) throws ShoppingCartDoesNotExistException, ProductDoesNotExistException {
        NewProductInCartData addProductToCartData = new NewProductInCartData(cartId, productIdData.getId());
        shoppingCartService.addProductToShoppingCart(addProductToCartData);
    }

    public ShoppingCartData find(@PathVariable("id") String id) throws ShoppingCartDoesNotExistException {
        return shoppingCartService.view(id);
    }
}
