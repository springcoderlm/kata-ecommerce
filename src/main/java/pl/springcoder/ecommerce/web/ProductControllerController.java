/*
 * Copyright (c) 2018. SpringCoder Lukasz Maslowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.springcoder.ecommerce.web;

import org.springframework.web.bind.annotation.*;
import pl.springcoder.ecommerce.application.ProductService;
import pl.springcoder.ecommerce.application.command.NewProductData;
import pl.springcoder.ecommerce.application.query.ProductData;
import pl.springcoder.ecommerce.domain.ProductAlreadyExistException;
import pl.springcoder.ecommerce.domain.ProductDoesNotExistException;

import java.util.List;

@RestController
public class ProductControllerController implements ProductControllerEndpoint {

    private ProductService productService;

    public ProductControllerController(ProductService productService) {
        this.productService = productService;
    }

    public ProductData add(@RequestBody NewProductData newProductData) throws ProductAlreadyExistException,
            ProductDoesNotExistException {
        String id = productService.addProduct(newProductData);
        return productService.productDetails(id);
    }

    public List<ProductData> all() {
        return productService.allProducts();
    }

    public ProductData find(@PathVariable("id") String id) throws ProductDoesNotExistException {
        return productService.productDetails(id);
    }
}
