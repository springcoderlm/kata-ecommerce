/*
 * Copyright (c) 2018. SpringCoder Lukasz Maslowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.springcoder.ecommerce.web;

import lombok.Builder;
import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import pl.springcoder.ecommerce.domain.ProductAlreadyExistException;
import pl.springcoder.ecommerce.domain.ProductDoesNotExistException;
import pl.springcoder.ecommerce.domain.ShoppingCartDoesNotExistException;

@RestController
@ControllerAdvice
public class ExceptionHandlingAdvice {

    @ExceptionHandler({ProductDoesNotExistException.class, ShoppingCartDoesNotExistException.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    public ErrorDto handleDoesNotExist(Exception e) {
        return new ErrorDto.ErrorDtoBuilder().message(e.getMessage()).exception(e.getClass().toString()).build();
    }

    @ExceptionHandler(ProductAlreadyExistException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorDto handleBadRequest(Exception e) {
        return new ErrorDto.ErrorDtoBuilder().message(e.getMessage()).exception(e.getClass().toString()).build();
    }

    @ExceptionHandler(Throwable.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public ErrorDto handle(Exception e) {
        return new ErrorDto.ErrorDtoBuilder().message(e.getMessage()).exception(e.getClass().toString()).build();
    }

    @Builder
    @Getter
    public static class ErrorDto {
        private String exception;
        private String message;
    }
}
