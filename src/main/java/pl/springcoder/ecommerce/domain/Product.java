/*
 * Copyright (c) 2018. SpringCoder Lukasz Maslowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.springcoder.ecommerce.domain;

import lombok.ToString;

import java.util.Objects;
import java.util.UUID;

@ToString
public class Product {

    private UUID id;
    private String name;
    private double price;

    public Product(String name) {
        setId();
        setName(name);
    }

    public Product(String name, double price) {
        setId();
        setName(name);
        setPrice(price);
    }

    private void setId() {
        this.id = UUID.randomUUID();
    }

    public UUID getId() {
        return id;
    }

    private void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    private void setName(String name) {
        if (name == null) {
            throw new IllegalArgumentException("name should be not null");
        }

        if (name.length() < 3 || name.length() > 50) {
            throw new IllegalArgumentException("name is not valid");
        }
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    private void setPrice(double price) {
        if (price < 0D) {
            throw new IllegalArgumentException("price is not valid");
        }
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Product)) return false;
        Product product = (Product) o;
        return Double.compare(product.getPrice(), getPrice()) == 0 &&
                Objects.equals(getId(), product.getId()) &&
                Objects.equals(getName(), product.getName());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getId(), getName(), getPrice());
    }
}
