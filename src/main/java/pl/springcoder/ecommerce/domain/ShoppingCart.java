/*
 * Copyright (c) 2018. SpringCoder Lukasz Maslowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.springcoder.ecommerce.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ShoppingCart {

    private UUID id;
    private List<Product> products;

    public ShoppingCart() {
        setId();
        setProducts();
    }

    public void addProduct(Product product) {
        getProducts().add(product);
    }

    public double calculateShoppingCartPrice() {
        return getProducts()
                .stream()
                .mapToDouble(Product::getPrice)
                .sum();
    }

    public List<ShoppingCartItem> viewShoppingCart() {
        Map<Product, Long> productGrouped = getProducts()
                .stream()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

        ArrayList<ShoppingCartItem> shoppingCartItems = new ArrayList<>();

        for (Map.Entry<Product, Long> entry : productGrouped.entrySet()) {
            shoppingCartItems.add(new ShoppingCartItem(entry.getKey(), entry.getValue().intValue()));
        }
        return shoppingCartItems;
    }

    public int size() {
        return getProducts().size();
    }

    public UUID getId() {
        return id;
    }

    private void setId(UUID id) {
        this.id = id;
    }

    private void setId() {
        this.id = UUID.randomUUID();
    }

    /**
     * intentional package-private access modifier useful for test
     */
    List<Product> getProducts() {
        return products;
    }

    private void setProducts(List<Product> products) {
        this.products = products;
    }

    private void setProducts() {
        this.products = new ArrayList<Product>();
    }
}
