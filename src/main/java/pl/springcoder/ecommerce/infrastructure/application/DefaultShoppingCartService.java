/*
 * Copyright (c) 2018. SpringCoder Lukasz Maslowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.springcoder.ecommerce.infrastructure.application;

import org.springframework.stereotype.Service;
import pl.springcoder.ecommerce.application.ShoppingCartService;
import pl.springcoder.ecommerce.application.command.NewProductInCartData;
import pl.springcoder.ecommerce.application.query.ProductData;
import pl.springcoder.ecommerce.application.query.ShoppingCartData;
import pl.springcoder.ecommerce.application.query.ShoppingCartItemData;
import pl.springcoder.ecommerce.domain.*;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
public class DefaultShoppingCartService implements ShoppingCartService {
    private ShoppingCartRepository shoppingCartRepository;
    private ProductRepository productRepository;

    public DefaultShoppingCartService(ShoppingCartRepository shoppingCartRepository, ProductRepository productRepository) {
        this.shoppingCartRepository = shoppingCartRepository;
        this.productRepository = productRepository;
    }

    public String newShoppingCart() {
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCartRepository.add(shoppingCart);
        return shoppingCart.getId().toString();
    }

    public void addProductToShoppingCart(NewProductInCartData addProductToShoppingCartData) throws ProductDoesNotExistException, ShoppingCartDoesNotExistException {
        String shoppingCartId = addProductToShoppingCartData.getShoppingCartId();
        String productId = addProductToShoppingCartData.getProductId();
        int quantity = addProductToShoppingCartData.getQuantity();

        ShoppingCart shoppingCart = shoppingCartRepository.find(UUID.fromString(shoppingCartId));
        Product product = productRepository.find(UUID.fromString(productId));

        IntStream.range(0, quantity).forEach(i -> {
            shoppingCart.addProduct(product);
        });
    }

    public ShoppingCartData view(String shoppingCartId) throws ShoppingCartDoesNotExistException {

        ShoppingCart shoppingCart = shoppingCartRepository.find(UUID.fromString(shoppingCartId));

        String id = shoppingCart.getId().toString();
        List<ShoppingCartItemData> shoppingCartItemDataList =
                shoppingCart
                        .viewShoppingCart()
                        .stream()
                        .map(shoppingCartItem -> {
                            Product product = shoppingCartItem.getProduct();
                            int quantity = shoppingCartItem.getQuantity();

                            ProductData productData = new ProductData(
                                    product.getId().toString(),
                                    product.getName(),
                                    product.getPrice()
                            );

                            return new ShoppingCartItemData(productData, quantity);
                        }).collect(Collectors.toList());

        return new ShoppingCartData(id, shoppingCartItemDataList, shoppingCart.calculateShoppingCartPrice());
    }

}
