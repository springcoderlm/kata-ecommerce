/*
 * Copyright (c) 2018. SpringCoder Lukasz Maslowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.springcoder.ecommerce.infrastructure.application;

import org.springframework.stereotype.Service;
import pl.springcoder.ecommerce.application.ProductService;
import pl.springcoder.ecommerce.application.command.NewProductData;
import pl.springcoder.ecommerce.application.query.ProductData;
import pl.springcoder.ecommerce.domain.Product;
import pl.springcoder.ecommerce.domain.ProductAlreadyExistException;
import pl.springcoder.ecommerce.domain.ProductDoesNotExistException;
import pl.springcoder.ecommerce.domain.ProductRepository;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class DefaultProductService implements ProductService {
    private ProductRepository productRepository;

    public DefaultProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public String addProduct(NewProductData newProductData) throws ProductAlreadyExistException {
        String name = newProductData.getName();
        throwExceptionIfProductAlreadyExists(name);
        double price = newProductData.getPrice();
        Product product = new Product(name, price);
        productRepository.add(product);
        return product.getId().toString();
    }

    private void throwExceptionIfProductAlreadyExists(String name) throws ProductAlreadyExistException {
        if (productRepository.exists(name)) {
            throw new ProductAlreadyExistException();
        }
    }

    public List<ProductData> allProducts() {
        return productRepository.findAll().stream().map(this::transform).collect(Collectors.toList());
    }

    public ProductData productDetails(String id) throws ProductDoesNotExistException {
        UUID uuid = UUID.fromString(id);
        Product product = productRepository.find(uuid);
        return transform(product);
    }

    private ProductData transform(Product product) {
        return new ProductData(
                product.getId().toString(),
                product.getName(),
                product.getPrice()
        );
    }
}
