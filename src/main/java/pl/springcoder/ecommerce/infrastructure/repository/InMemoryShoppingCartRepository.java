/*
 * Copyright (c) 2019. SpringCoder Lukasz Maslowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.springcoder.ecommerce.infrastructure.repository;

import org.springframework.stereotype.Repository;
import pl.springcoder.ecommerce.domain.ShoppingCart;
import pl.springcoder.ecommerce.domain.ShoppingCartDoesNotExistException;
import pl.springcoder.ecommerce.domain.ShoppingCartRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Repository
public class InMemoryShoppingCartRepository implements ShoppingCartRepository {

    private List<ShoppingCart> shoppingCarts = new ArrayList<>();

    @Override
    public ShoppingCart find(UUID id) throws ShoppingCartDoesNotExistException {
        return shoppingCarts.stream().filter(s -> s.getId().equals(id)).findFirst().orElseThrow(ShoppingCartDoesNotExistException::new);
    }

    @Override
    public List<ShoppingCart> findAll() {
        return shoppingCarts;
    }

    @Override
    public void add(ShoppingCart shoppingCart) {
        shoppingCarts.add(shoppingCart);
    }

    @Override
    public void remove(ShoppingCart shoppingCart) {
        shoppingCarts.remove(shoppingCart);
    }
}
