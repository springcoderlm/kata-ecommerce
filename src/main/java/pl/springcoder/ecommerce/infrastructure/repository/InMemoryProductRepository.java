/*
 * Copyright (c) 2019. SpringCoder Lukasz Maslowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.springcoder.ecommerce.infrastructure.repository;

import org.springframework.stereotype.Repository;
import pl.springcoder.ecommerce.domain.Product;
import pl.springcoder.ecommerce.domain.ProductDoesNotExistException;
import pl.springcoder.ecommerce.domain.ProductRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Repository
public class InMemoryProductRepository implements ProductRepository {

    private List<Product> products = new ArrayList<>();

    @Override
    public Product find(UUID id) throws ProductDoesNotExistException {
        return products.stream().filter(p -> p.getId().equals(id)).findFirst().orElseThrow(ProductDoesNotExistException::new);
    }


    @Override
    public List<Product> findAll() {
        return products;
    }

    @Override
    public void add(Product product) {
        products.add(product);
    }

    @Override
    public void remove(Product product) {
        products.remove(product);
    }

    @Override
    public boolean exists(String name) {
        return products.stream().filter(p -> p.getName().equals(name)).count() == 1;
    }
}
