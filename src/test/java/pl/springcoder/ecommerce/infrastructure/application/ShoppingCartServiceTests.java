/*
 * Copyright (c) 2018. SpringCoder Lukasz Maslowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.springcoder.ecommerce.infrastructure.application;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import pl.springcoder.ecommerce.application.ShoppingCartService;
import pl.springcoder.ecommerce.application.command.NewProductInCartData;
import pl.springcoder.ecommerce.application.query.ProductData;
import pl.springcoder.ecommerce.application.query.ShoppingCartData;
import pl.springcoder.ecommerce.domain.Product;
import pl.springcoder.ecommerce.domain.ProductDoesNotExistException;
import pl.springcoder.ecommerce.domain.ShoppingCart;
import pl.springcoder.ecommerce.domain.ShoppingCartDoesNotExistException;
import pl.springcoder.ecommerce.infrastructure.repository.InMemoryProductRepository;
import pl.springcoder.ecommerce.infrastructure.repository.InMemoryShoppingCartRepository;

import java.util.UUID;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class ShoppingCartServiceTests {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    private ShoppingCartService shoppingCartService;
    private InMemoryShoppingCartRepository shoppingCartRepository;
    private InMemoryProductRepository productRepository;

    @Before
    public void setup() {
        shoppingCartRepository = new InMemoryShoppingCartRepository();
        productRepository = new InMemoryProductRepository();
        shoppingCartService = new DefaultShoppingCartService(
                shoppingCartRepository,
                productRepository
        );
    }

    @Test
    public void shouldCreateNewShoppingCart() throws ShoppingCartDoesNotExistException {
        //when
        String id = shoppingCartService.newShoppingCart();

        //then
        ShoppingCart shoppingCart = shoppingCartRepository.find(UUID.fromString(id));
        assertThat(shoppingCart.getId().toString(), is(id));
    }


    @Test
    public void shouldThrowExceptionWhenAddingProductToShoppingCartDoesNotExist() throws ShoppingCartDoesNotExistException, ProductDoesNotExistException {
        //expected
        expectedException.expect(ProductDoesNotExistException.class);

        //given
        String shoppingCartId = shoppingCartService.newShoppingCart();
        String notExistsProductId = UUID.randomUUID().toString();
        NewProductInCartData newProductInCartData = new NewProductInCartData(shoppingCartId, notExistsProductId);

        //when
        shoppingCartService.addProductToShoppingCart(newProductInCartData);
    }

    @Test
    public void shouldThrowExeceptionWhenShoppingCartDoesNotExists() throws ShoppingCartDoesNotExistException, ProductDoesNotExistException {
        //expected
        expectedException.expect(ShoppingCartDoesNotExistException.class);

        //given
        String shoppingCartId = UUID.randomUUID().toString();
        String notExistsProductId = UUID.randomUUID().toString();
        NewProductInCartData newProductInCartData = new NewProductInCartData(shoppingCartId, notExistsProductId);

        //when
        shoppingCartService.addProductToShoppingCart(newProductInCartData);
    }

    @Test
    public void shouldAddProductToShoppingCart() throws ShoppingCartDoesNotExistException, ProductDoesNotExistException {
        //given
        ShoppingCart shoppingCart = new ShoppingCart();
        String shoppingCartId = shoppingCart.getId().toString();
        shoppingCartRepository.add(shoppingCart);

        Product product = new Product("Product#1", 10D);
        String productId = product.getId().toString();
        productRepository.add(product);

        //when
        NewProductInCartData addProductToShoppingCartData = new NewProductInCartData(shoppingCartId, productId);
        shoppingCartService.addProductToShoppingCart(addProductToShoppingCartData);

        //then
        ShoppingCartData view = shoppingCartService.view(shoppingCartId);
        assertThat(view.getId(), is(shoppingCartId));
        assertThat(view.getPrice(), is(10D));

        ProductData expectedProductData = new ProductData(productId, "Product#1", 10D);
        assertThat(view.getShoppingCartItems(), hasItem(hasProperty("productData", is(expectedProductData))));
        assertThat(view.getShoppingCartItems(), hasSize(1));
    }

    @Test
    public void shouldReturnPresentationOfShoppingCart() throws ShoppingCartDoesNotExistException, ProductDoesNotExistException {
        //given
        ShoppingCart shoppingCart = new ShoppingCart();
        String shoppingCartId = shoppingCart.getId().toString();
        shoppingCartRepository.add(shoppingCart);

        String name = "Product#1";
        Product product = new Product(name, 10D);
        String productId = product.getId().toString();
        productRepository.add(product);
        NewProductInCartData addProductToShoppingCartData = new NewProductInCartData(shoppingCartId, productId);
        shoppingCartService.addProductToShoppingCart(addProductToShoppingCartData);
        shoppingCartService.addProductToShoppingCart(addProductToShoppingCartData);
        shoppingCartService.addProductToShoppingCart(addProductToShoppingCartData);
        shoppingCartService.addProductToShoppingCart(addProductToShoppingCartData);

        //when
        shoppingCartService.view(shoppingCartId);

        //then
        ShoppingCartData view = shoppingCartService.view(shoppingCartId);
        assertThat(view.getId(), is(shoppingCartId));
        assertThat(view.getPrice(), is(40D));

        ProductData expectedProductData = new ProductData(productId, "Product#1", 10D);
        assertThat(view.getShoppingCartItems(), hasItem(hasProperty("productData", is(expectedProductData))));
        assertThat(view.getShoppingCartItems(), hasItem(hasProperty("quantity", is(4))));
        assertThat(view.getShoppingCartItems(), hasSize(1));
    }

    @Test
    public void shouldThrowExceptionWhenShoppingCartDoesNotExist() throws ShoppingCartDoesNotExistException {
        expectedException.expect(ShoppingCartDoesNotExistException.class);
        shoppingCartService.view(UUID.randomUUID().toString());
    }

    @Test
    public void shouldThrowExceptionIfShoppingCartIsNotValid() throws ShoppingCartDoesNotExistException {
        expectedException.expect(IllegalArgumentException.class);
        shoppingCartService.view("12345");
    }
}

