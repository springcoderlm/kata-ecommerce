/*
 * Copyright (c) 2018. SpringCoder Lukasz Maslowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.springcoder.ecommerce.infrastructure.application;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import pl.springcoder.ecommerce.App;
import pl.springcoder.ecommerce.application.ProductService;
import pl.springcoder.ecommerce.application.command.NewProductData;
import pl.springcoder.ecommerce.application.query.ProductData;
import pl.springcoder.ecommerce.domain.ProductAlreadyExistException;
import pl.springcoder.ecommerce.domain.ProductDoesNotExistException;

import java.util.UUID;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
//@SpringBootTest
@ContextConfiguration(classes = {App.class})
@ActiveProfiles("testing")
public class ProductServiceSpringIntegrationTests {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    @Autowired
    private ProductService productService;

    @Test
    public void shouldBeNotNull() {
        assertNotNull(productService);
    }

    @Test
    public void shouldCreateNewProduct() throws ProductDoesNotExistException, ProductAlreadyExistException {
        NewProductData newProductData = new NewProductData("Product#1", 1.2);
        String uuid = productService.addProduct(newProductData);

        ProductData productData = productService.productDetails(uuid);

        assertThat("Product#1", is(productData.getName()));
        assertThat(1.2, is(productData.getPrice()));
        assertThat(uuid, is(productData.getId()));
    }

    @Test
    public void shouldThrowExceptionIfProductDoesNotExists() throws ProductDoesNotExistException {
        //expected
        expectedException.expect(ProductDoesNotExistException.class);

        //when
        productService.productDetails(UUID.randomUUID().toString());
    }

    @Test
    public void shouldThrowExceptionIfProductNameIsAlreadyExists() throws ProductAlreadyExistException {
        //expected
        expectedException.expect(ProductAlreadyExistException.class);

        //when
        productService.addProduct(new NewProductData("Product#1", 1));
        productService.addProduct(new NewProductData("Product#1", 1));
    }
}
