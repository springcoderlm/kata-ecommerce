/*
 * Copyright (c) 2018. SpringCoder Lukasz Maslowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.springcoder.ecommerce.web;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import pl.springcoder.ecommerce.App;
import pl.springcoder.ecommerce.application.ProductService;
import pl.springcoder.ecommerce.application.ShoppingCartService;
import pl.springcoder.ecommerce.application.command.NewProductInCartData;
import pl.springcoder.ecommerce.application.query.ProductData;
import pl.springcoder.ecommerce.application.query.ShoppingCartData;
import pl.springcoder.ecommerce.application.query.ShoppingCartItemData;
import pl.springcoder.ecommerce.domain.ShoppingCartDoesNotExistException;
import pl.springcoder.ecommerce.web.dto.ProductIdData;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {App.class, MockedTestContext.class})
@WebAppConfiguration
public class ShoppingCartControllerTests {

    @Autowired
    WebApplicationContext context;

    @Autowired
    private ShoppingCartService shoppingCartService;

    @Autowired
    private ProductService productService;

    private MockMvc mockMvc;

    @Before
    public void before() {
        Mockito.reset(shoppingCartService, productService);
        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
    }

    @Test
    public void shouldCreateShoppingCart() throws Exception {
        //given
        List<ShoppingCartItemData> shoppingCartItems = new ArrayList<>();

        ShoppingCartData shoppingCartData = new ShoppingCartData(
                "12345",
                shoppingCartItems,
                0
        );

        //when
        when(shoppingCartService.newShoppingCart()).thenReturn("12345");
        when(shoppingCartService.view(Mockito.any())).thenReturn(shoppingCartData);

        //then
        mockMvc.perform(
                MockMvcRequestBuilders.post("/api/carts")
                        .contentType(TestUtil.APPLICATION_JSON_UTF8)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id", is("12345")))
                .andExpect(jsonPath("$.price", is(0D)))
                .andExpect(jsonPath("$.shoppingCartItems", is(shoppingCartItems)));
    }

    @Test
    public void shouldAddProductToShoppingCart() throws Exception {
        //given
        String shoppingCartId = "12345";
        String productId = "11111";

        //when
        ProductIdData productDataId = new ProductIdData(productId);

        //then
        mockMvc.perform(
                MockMvcRequestBuilders.post("/api/carts/" + shoppingCartId + "/products")
                        .content(TestUtil.convertObjectToJsonBytes(productDataId))
                        .contentType(TestUtil.APPLICATION_JSON_UTF8)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andDo(print());

        NewProductInCartData addProductToCartData = new NewProductInCartData(shoppingCartId, productId);
        verify(shoppingCartService, times(1)).addProductToShoppingCart(addProductToCartData);
    }

    @Test
    public void shouldReturnShoppingCartDetails() throws Exception {
        //given
        List<ShoppingCartItemData> shoppingCartItems = new ArrayList<>();
        shoppingCartItems.add(new ShoppingCartItemData(
                new ProductData("11111", "Product#1", 1), 5
        ));
        shoppingCartItems.add(new ShoppingCartItemData(
                new ProductData("22222", "Product#2", 2), 2
        ));

        ShoppingCartData shoppingCartData = new ShoppingCartData(
                "12345",
                shoppingCartItems,
                100
        );

        //when
        when(shoppingCartService.view("12345")).thenReturn(shoppingCartData);

        //then
        mockMvc
                .perform(MockMvcRequestBuilders.get("/api/carts/" + "12345"))
                .andDo(print())
                .andExpect(jsonPath("$.id", is("12345")))
                .andExpect(jsonPath("$.price", is(100D)))
                .andExpect(jsonPath("$.shoppingCartItems.[0].productData.id", is("11111")))
                .andExpect(jsonPath("$.shoppingCartItems.[0].productData.name", is("Product#1")))
                .andExpect(jsonPath("$.shoppingCartItems.[0].productData.price", is(1.0D)))
                .andExpect(jsonPath("$.shoppingCartItems.[0].quantity", is(5)))
                .andExpect(jsonPath("$.shoppingCartItems.[1].productData.id", is("22222")))
                .andExpect(jsonPath("$.shoppingCartItems.[1].productData.name", is("Product#2")))
                .andExpect(jsonPath("$.shoppingCartItems.[1].productData.price", is(2.0D)))
                .andExpect(jsonPath("$.shoppingCartItems.[1].quantity", is(2)));
    }

    @Test
    public void shouldThrowExceptionIfShoppingCartDoesNotExists() throws Exception {
        //when
        when(shoppingCartService.view(Mockito.any())).thenThrow(new ShoppingCartDoesNotExistException());

        mockMvc.perform(MockMvcRequestBuilders.get("/api/carts/" + UUID.randomUUID().toString())
                .accept(TestUtil.APPLICATION_JSON_UTF8)
                .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(MockMvcResultMatchers.jsonPath("$.exception", is(ShoppingCartDoesNotExistException.class.toString())));

    }
}
