/*
 * Copyright (c) 2018. SpringCoder Lukasz Maslowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.springcoder.ecommerce.web;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import pl.springcoder.ecommerce.App;
import pl.springcoder.ecommerce.application.ProductService;
import pl.springcoder.ecommerce.application.command.NewProductData;
import pl.springcoder.ecommerce.application.query.ProductData;
import pl.springcoder.ecommerce.domain.ProductAlreadyExistException;
import pl.springcoder.ecommerce.domain.ProductDoesNotExistException;

import java.util.ArrayList;
import java.util.UUID;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {App.class, MockedTestContext.class})
@WebAppConfiguration
public class ProductControllerTests {

    @Autowired
    WebApplicationContext wac;

    private MockMvc mockMvc;

    @Autowired
    private ProductService productService;

    @Before
    public void setup() {
        Mockito.reset(productService);
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
    }

    @Test
    public void shouldBeNotNull() {
        assertNotNull(wac);
    }

    @Test
    public void shouldCreateProduct() throws Exception {
        //given
        NewProductData newProduct = new NewProductData("Product#99", 1.2);

        //when
        when(productService.addProduct(newProduct)).thenReturn("12345");
        when(productService.productDetails(Mockito.anyString())).thenReturn(new ProductData("12345", "Product#99", 1.2));

        // when/then
        MvcResult mvcResult = mockMvc.perform(
                post("/api/products")
                        .content(TestUtil.convertObjectToJsonBytes(newProduct))
                        .contentType(TestUtil.APPLICATION_JSON_UTF8)
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name", is("Product#99")))
                .andExpect(jsonPath("$.price", is(1.2D)))
                .andExpect(jsonPath("$.id", is("12345")))
                .andReturn();
    }

    @Test
    public void shouldThrowProductAlreadyExistException() throws Exception {
        //given
        NewProductData newProduct = new NewProductData("Product#1", 1.2);

        //when
        doThrow(new ProductAlreadyExistException())
                .when(productService)
                .addProduct(newProduct);

        mockMvc.perform(post("/api/products")
                .content(TestUtil.convertObjectToJsonBytes(newProduct))
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.jsonPath("$.exception", is(ProductAlreadyExistException.class.toString())))
        ;
    }

    @Test
    public void shouldReturnAllProduct() throws Exception {
        //given
        ProductData p1 = new ProductData("11111", "Product#1", 1);
        ProductData p2 = new ProductData("22222", "Product#2", 2);
        ProductData p3 = new ProductData("33333", "Product#3", 3);
        ArrayList<ProductData> products = new ArrayList<>();
        products.add(p1);
        products.add(p2);
        products.add(p3);

        //when
        when(productService.allProducts()).thenReturn(products);

        //then
        mockMvc.perform(get("/api/products")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id", is("11111")))
                .andExpect(jsonPath("$[1].id", is("22222")))
                .andExpect(jsonPath("$[2].id", is("33333")));
    }

    @Test
    public void shouldReturnProductDetails() throws Exception {
        //given
        ProductData productData = new ProductData("12345", "Product#1", 1);

        //when
        when(productService.productDetails(anyString())).thenReturn(productData);

        //then
        mockMvc.perform(
                get("/api/products/" + "12345")
                        .contentType(TestUtil.APPLICATION_JSON_UTF8)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is("12345")))
                .andExpect(jsonPath("$.name", is("Product#1")))
                .andExpect(jsonPath("$.price", is(1D)));
    }

    @Test
    public void shouldThrowExceptionIfProductDoesNotExist() throws Exception {
        String id = UUID.randomUUID().toString();

        Mockito.when(productService.productDetails(Mockito.any())).thenThrow(new ProductDoesNotExistException());

        mockMvc.perform(get("/api/products/" + id)
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isNotFound())
                .andExpect(MockMvcResultMatchers.jsonPath("$.exception",
                        is(ProductDoesNotExistException.class.toString())))
        ;
    }
}
