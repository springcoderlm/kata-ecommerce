/*
 * Copyright (c) 2018. SpringCoder Lukasz Maslowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.springcoder.ecommerce.domain;

import org.junit.Test;

import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsCollectionContaining.hasItem;
import static org.junit.Assert.assertThat;

public class ShoppingCartTests {

    private Product product1 = new Product("Product#1", 10D);
    private Product product2 = new Product("Product#2", 20D);
    private Product product3 = new Product("Product#3", 30D);


    @Test
    public void addProductToShoppingCart() {
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addProduct(product1);
        shoppingCart.addProduct(product2);
        shoppingCart.addProduct(product3);

        assertThat(shoppingCart.size(), is(3));
        assertThat(shoppingCart.getProducts(), hasItem(product1));
        assertThat(shoppingCart.getProducts(), hasItem(product2));
        assertThat(shoppingCart.getProducts(), hasItem(product3));
        assertThat(shoppingCart.calculateShoppingCartPrice(), is(60D));
    }

    @Test
    public void addOneProductTwice() {
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addProduct(product1);
        shoppingCart.addProduct(product1);

        assertThat(shoppingCart.size(), is(2));
        assertThat(shoppingCart.getProducts(), hasItem(product1));
        assertThat(shoppingCart.calculateShoppingCartPrice(), is(20D));
    }

    @Test
    public void viewShoppingCart() {
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addProduct(product1);
        shoppingCart.addProduct(product1);
        shoppingCart.addProduct(product2);
        shoppingCart.addProduct(product2);
        shoppingCart.addProduct(product2);
        shoppingCart.addProduct(product2);
        shoppingCart.addProduct(product3);
        shoppingCart.addProduct(product3);

        List<ShoppingCartItem> shoppingCartItems = shoppingCart.viewShoppingCart();

        assertThat(shoppingCartItems.size(), is(3));
        assertThat(shoppingCartItems, hasItem(new ShoppingCartItem(product1, 2)));
        assertThat(shoppingCartItems, hasItem(new ShoppingCartItem(product2, 4)));
        assertThat(shoppingCartItems, hasItem(new ShoppingCartItem(product3, 2)));
    }
}
