/*
 * Copyright (c) 2018. SpringCoder Lukasz Maslowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.springcoder.ecommerce.domain;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

public class ProductTests {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void shouldCreateProduct() {
        String name = "Product#1";
        double price = 10;
        Product product = new Product(name, price);

        assertNotNull(product.getId());
        assertThat(product.getName(), is(name));
        assertThat(product.getPrice(), is(price));
    }

    @Test
    public void shouldThrowExceptionWhenCreateProductWithTooShortName() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("name is not valid");

        String name = "Pr";
        double price = 10;

        new Product(name, price);
    }

    @Test
    public void shouldThrowExceptionWhenCreateProductWithTooLongName() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("name is not valid");

        String name = new String(new byte[51]);
        double price = 10;

        new Product(name, price);
    }

    @Test
    public void shouldThrowExceptionWhenCreateProductWithWrongPrice() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("price is not valid");

        String name = "Product#1";
        double price = -1;

        new Product(name, price);
    }
}
